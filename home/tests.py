from django.test import TestCase, Client
from django.urls import resolve, reverse
from produk.models import AddProduk
from produk.views import listproduk, addproduk
from .views import index


class HomeTest(TestCase):
    def setUp(self):
        self.produk = AddProduk.objects.create(
            nama="A", harga="20000", stok="2", penjual="B", email="B@gmail.com", deskripsi="Barang")
        self.addproduk = reverse('produk:addproduk')
        self.listproduk = reverse('produk:listproduk')

    def test_urls_addproduk_test(self):
        found = resolve(self.addproduk)
        self.assertEqual(found.func, addproduk)

    def test_urls_listproduk_test(self):
        found = resolve(self.listproduk)
        self.assertEqual(found.func, listproduk)

    def test_url_home_is_exist(self):
        response = Client().post('')
        self.assertEqual(response.status_code, 200)

    def test_home_using_index_template(self):
        response = Client().post('')
        self.assertTemplateUsed(response, 'index.html')

    def test_home_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_navbar_homepage(self):
        response = Client().post('')
        self.assertContains(response, "<nav")

    def test_footer_homepage(self):
        response = Client().get('')
        self.assertContains(response, '</footer>')

    def test_medisal_is_exist(self):
        response = Client().get('')
        self.assertContains(response, 'MEDISALE')

    def test_image_homepage(self):
        response = Client().get('')
        self.assertContains(response, 'img')

    def test_prevention_is_exist(self):
        response = Client().get('')
        self.assertContains(response, 'PENCEGAHAN TERPAPAR VIRUS CORONA')

    def test_description_is_exist(self):
        response = Client().get('')
        self.assertContains(response, 'Medisale adalah')
