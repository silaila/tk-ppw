from django.shortcuts import render
from produk.models import AddProduk
import random

# Create your views here.
def index(request):
    produk = AddProduk.objects.all()
    if len(produk) > 3:
        random_items = random.choices(list(produk), k=3)
        context = {
            'produk' : random_items
        }
        return render(request, 'index.html', context)
    else:
        context = {
            'produk' : produk
        }
        return render(request, 'index.html', context)
    random_items = random.sample(list(produk),3)
    context = {
        'produk' : random_items
    }
    return render(request, 'index.html', context)

def handler404(request, *args, **kwargs):
    return render(request, '404.html')

def handler500(request, *args, **kwargs):
    return render(request, '500.html')
