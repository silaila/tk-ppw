# Generated by Django 3.1.2 on 2020-11-11 14:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bantuan', '0002_auto_20201111_2105'),
    ]

    operations = [
        migrations.AddField(
            model_name='modelbantuan',
            name='answer',
            field=models.TextField(blank=True),
        ),
    ]
