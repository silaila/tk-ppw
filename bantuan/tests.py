from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps
from .models import ModelBantuan
from .views import addPertanyaan
from .forms import FormBantuan
from .apps import BantuanConfig
# Create your tests here.

class UnitTest(TestCase):
    def test_url_bantuan_is_exist(self):
        response = Client().post('/bantuan/')
        self.assertEqual(response.status_code, 200)
    
    def test_bantuan_using_bantuan_template(self):
        response = Client().post('/bantuan/')
        self.assertTemplateUsed(response, 'bantuan.html')

    def test_bantuan_using_addQ_func(self):
        found = resolve('/bantuan/')
        self.assertEqual(found.func, addPertanyaan)

    def test_model_can_create_bantuan(self):
        newQuestion = ModelBantuan.objects.create(name="vioren", question="apa ya?", answer="apa")
        counting_all_question = ModelBantuan.objects.all().count()
        self.assertEqual(counting_all_question, 1)
        question_str = ModelBantuan.objects.get(pk=1)
        self.assertEqual(str(question_str), question_str.question)
    
    def test_can_save_POST_request(self):
        respone = self.client.post('/bantuan/', {"name" :"vioren", "question" : "apa ya?", "answer" :"apa"})
        counting_all_question = ModelBantuan.objects.all().count()
        self.assertEqual(counting_all_question, 1)

        self.assertEqual(respone.status_code, 200)

        new_response = self.client.get('/bantuan/')
        html_response = new_response.content.decode("utf8")
        self.assertIn("apa ya?", html_response)
