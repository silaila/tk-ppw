from django import forms
from .models import ModelBantuan

class FormBantuan(forms.Form):
    name = forms.CharField(
        label = "Nama",
        max_length = 50,
        widget = forms.TextInput(
            attrs={
            'class' : 'form-control',
            'placeholder' : 'Medisa',
            'type' : 'text',
            'required' : True,
            'style': 'margin-bottom: 10px;'
            }
        )
    )
    question = forms.CharField(
        label = "Pertanyaan",
        max_length = 300,
        widget = forms.Textarea(
            attrs={
            'class' : 'form-control',
            'placeholder' : 'Apakah saya bisa membeli grosir?',
            'type' : 'text',
            'required' : True,
            'style': 'margin-bottom: 10px;'
            }
        )
    )
