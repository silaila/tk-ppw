from django.shortcuts import render
from .forms import FormBantuan
from .models import ModelBantuan

# Create your views here.
def addPertanyaan(request):
    formBantuan = FormBantuan()
    if request.method == "POST":
        formBantuan_input = FormBantuan(request.POST)
        if formBantuan_input.is_valid():
            data = formBantuan_input.cleaned_data
            bantuan_input = ModelBantuan()
            bantuan_input.name = data['name']
            bantuan_input.question = data['question']
            bantuan_input.save()
            pertanyaan = ModelBantuan.objects.all()
            return render(request, 'bantuan.html', {'form': formBantuan, 'pertanyaan': pertanyaan, 'status': 'failed'})
    pertanyaan = ModelBantuan.objects.all()
    return render(request, 'bantuan.html', {'form': formBantuan, 'pertanyaan': pertanyaan,})

# def listPertanyaan(request):
#     pertanyaan = ModelBantuan.objects.all()
#     return render(request, 'bantuan.html', {'pertanyaan': pertanyaan})