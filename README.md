[![pipeline status](https://gitlab.com/silaila/tk-ppw/badges/master/pipeline.svg)](https://gitlab.com/silaila/tk-ppw/commits/master) [![coverage report](https://gitlab.com/silaila/tk-ppw/badges/master/coverage.svg)](https://gitlab.com/silaila/tk-ppw/commits/master)

# Tugas Kelompok 1 PPW
Kelas: PPW-D
Kelompok: D07

### Anggota:
1. Herbiyona
2. Vioren Paramitta Adithana
3. Muhammad Urwatil Wutsqo
4. Siti Nurlaila

### Link Herokuapp:
https://medisale.herokuapp.com/

### Tentang Aplikasi:
MEDISALE
Medisale adalah aplikasi/website yang menjual alat-alat medis untuk penanganan dan pencegahan corona. Pasar utama kami adalah tenaga kesehatan maupun orang-orang yang masih aktif berkegiatan di tengah pandemi corona. Manfaat dari MEDISALE adalah mempermudah masyarakat dalam membeli alat perlindungan diri dan alat-alat medis.

### Fitur yang akan Diimplementasikan:
Fitur-fitur yang ada di MEDISALE:  
-Navbar untuk memudahkan navigasi (Vioren, Herbiyona, Wutsqo)  
-Home untuk landing page (Herbiyona, Wutsqo, Vioren)  
-Page bantuan untuk menjawab pertanyaan dari user (Vioren)  
-Page keranjang untuk membeli barang yang diinginkan (Wutsqo)  
-Page detail produk dengan testimoni dari barangnya (Herbiyona)  
-Page untuk menambah produk untuk dijual (Siti Nurlaila)  
-Page untuk menampilkan produk yang tersedia (Siti Nurlaila)