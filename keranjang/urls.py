from django.urls import path
from . import views

app_name = 'keranjang'

urlpatterns = [
    path('', views.keranjang, name='keranjang'),
    path('checkout', views.checkout, name='checkout'),
    path('add/<int:detail_id>', views.addToKeranjang, name='tambah'),
    path('addPOST', views.addPOST, name='addPOST'),
    path('delPOST', views.delPOST, name='delPOST'),
    path('incPOST', views.incPOST, name='incPOST'),
    path('decPOST', views.decPOST, name='decPOST'),
]