from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.db import IntegrityError
from produk.models import AddProduk
from .models import ProdukDiKeranjang, DetailPengiriman, Visitor, ProdukAkanDikirim, PaymentMethod, Courier, StatusPengiriman
from .forms import FormPengiriman
import datetime
import uuid

def keranjang(request):
    visitor = getVisitor(request)

    produk = ProdukDiKeranjang.objects.filter(visitor=visitor)
    form = FormPengiriman()
    
    totalHarga = sum([(x.product.harga * x.jumlah) for x in produk])
    
    response = {
        'produk' : produk,
        'form' : form,
        'totalHarga' : totalHarga,
        'hide_nav_2' : True,
        'hide_footer': True,
    }
    return render(request, 'keranjang/keranjang.html', response)

def checkout(request):
    visitor = getVisitor(request)

    if request.method=='POST':

        if ProdukDiKeranjang.objects.filter(visitor=visitor).count() != 0:

            produk_di_keranjang = ProdukDiKeranjang.objects.filter(visitor=visitor)
            totalHarga = sum([(x.product.harga * x.jumlah) for x in produk_di_keranjang])
            formCheckout = FormPengiriman(request.POST or None)

            if formCheckout.is_valid():

                detailpengiriman = DetailPengiriman.objects.create(
                    nama = formCheckout.cleaned_data['nama'],
                    telpon = formCheckout.cleaned_data['telpon'],
                    email = formCheckout.cleaned_data['email'],
                    alamat = formCheckout.cleaned_data['alamat'],
                    total_harga = totalHarga,
                    kurir = Courier.objects.get(name=formCheckout.cleaned_data['kurir']),
                    metode_pembayaran = PaymentMethod.objects.get(name=formCheckout.cleaned_data['metode_pembayaran']),
                    status_pengiriman = StatusPengiriman.objects.get(name='Menunggu Pembayaran')
                )

                for p in produk_di_keranjang:
                    ProdukAkanDikirim.objects.create(
                        product = p.product,
                        detailpengiriman = detailpengiriman,
                        jumlah = p.jumlah
                    )

                    produk = AddProduk.objects.get(id=p.product.id)
                    produk.stok -= p.jumlah
                    produk.save()

                produk_di_keranjang.delete()
                response = {
                    'detail' : detailpengiriman,
                    'hide_nav_2' : True,
                    'hide_footer': True,
                }


                return render(request, 'keranjang/checkout.html', response)
        else:
            return HttpResponseBadRequest('ProdukDiKeranjang instance not found')
    else:
        return redirect('keranjang:keranjang')

def addToKeranjang(request, detail_id):
    try:
        produk = AddProduk.objects.get(id=detail_id)
        visitor = getVisitor(request)

        if len(ProdukDiKeranjang.objects.filter(product=produk, visitor=visitor)) == 0:
            ProdukDiKeranjang.objects.create(
                product = produk,
                visitor = visitor,
                jumlah = 1,
            )
            
        return redirect('keranjang:keranjang')
    except AddProduk.DoesNotExist:
        return HttpResponseBadRequest('Produk instance not found')

def addPOST(request):
    if request.method == 'POST':
        try:
            detail_id = request.POST.get('detail_id')
            produk = AddProduk.objects.get(id=detail_id)
            visitor = getVisitor(request)

            if len(ProdukDiKeranjang.objects.filter(product=produk, visitor=visitor)) == 0:
                ProdukDiKeranjang.objects.create(
                    product = produk,
                    visitor = visitor,
                    jumlah = 1,
                )
            
            return redirect('keranjang:keranjang')

        except AddProduk.DoesNotExist:
            return HttpResponseBadRequest('Produk instance not found')
    
    else:
        return HttpResponseBadRequest('GET method is not allowed')

def delPOST(request):
    if request.method == 'POST':
        try:
            detail_id = request.POST.get('detail_id')
            produk = AddProduk.objects.get(id=detail_id)
            visitor = getVisitor(request)

            ProdukDiKeranjang.objects.get(product=produk, visitor=visitor).delete()
            return redirect('keranjang:keranjang')

        except ProdukDiKeranjang.DoesNotExist:
            return HttpResponseBadRequest('ProdukDiKeranjang instance not found')
        
        except AddProduk.DoesNotExist:
            return HttpResponseBadRequest('AddProduk instance not found')

    else:
        return HttpResponseBadRequest('GET method is not allowed')
       
def incPOST(request):
    if request.method == 'POST':
        try:
            detail_id = request.POST.get('detail_id')
            produk = AddProduk.objects.get(id=detail_id)
            visitor = getVisitor(request)

            produk_di_keranjang = ProdukDiKeranjang.objects.get(product=produk, visitor=visitor)

            if produk_di_keranjang.jumlah == produk.stok:
                return HttpResponseBadRequest('ProdukDiKeranjang.jumlah cannot exceeds stok produk')

            else:
                produk_di_keranjang.jumlah += 1
                produk_di_keranjang.save()
                return redirect('keranjang:keranjang')

        except ProdukDiKeranjang.DoesNotExist:
            return HttpResponseBadRequest('ProdukDiKeranjang instance not found')

        except AddProduk.DoesNotExist:
            return HttpResponseBadRequest('AddProduk instance not found')

    else:
        return HttpResponseBadRequest('GET method is not allowed')

def decPOST(request):
    if request.method=='POST':
        try:
            detail_id = request.POST.get('detail_id')
            produk = AddProduk.objects.get(id=detail_id)
            visitor = getVisitor(request)

            produk_di_keranjang = ProdukDiKeranjang.objects.get(product=produk, visitor=visitor)

            if produk_di_keranjang.jumlah == 1:
                return HttpResponseBadRequest('ProdukDiKeranjang.jumlah cannot be zero')

            else:
                produk_di_keranjang.jumlah -= 1
                produk_di_keranjang.save()
                return redirect('keranjang:keranjang')

        except ProdukDiKeranjang.DoesNotExist:
            return HttpResponseBadRequest('ProdukDiKeranjang instance not found') 

        except AddProduk.DoesNotExist:
            return HttpResponseBadRequest('AddProduk instance not found')
    
    else:
        return HttpResponseBadRequest('GET method is not allowed')

def getVisitor(request):
    if request.session.get('sesi') == None:
        sesi = newVisitor(request)
    else:
        sesi = request.session['sesi']

    try:
        return Visitor.objects.get(id=sesi)

    except Visitor.DoesNotExist:
        sesi = newVisitor(request)
        return Visitor.objects.get(id=sesi)

def newVisitor(request):
    id = uuid.uuid4()
    request.session['sesi'] = str(id)
    try:
        Visitor.objects.create(id=id)
    except IntegrityError:
        Visitor.objects.get(id=id).delete()
        Visitor.objects.create(id=id)

    return id
