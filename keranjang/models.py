from django.db import models
from produk.models import AddProduk
import uuid

class Visitor(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    def __str__(self):
        return "{}".format(self.id)

class ProdukDiKeranjang(models.Model):
    product = models.ForeignKey(AddProduk, on_delete=models.CASCADE)
    visitor = models.ForeignKey(Visitor, on_delete=models.CASCADE, related_name='produkdikeranjang')
    jumlah = models.PositiveIntegerField()
    
    def __str__(self):
        return f'{self.product.nama} di {self.visitor}'

class PaymentMethod(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Courier(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class StatusPengiriman(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class DetailPengiriman(models.Model):
    nama = models.CharField(max_length=30)
    telpon = models.CharField(max_length=15)
    email = models.EmailField()
    alamat = models.CharField(max_length=100)
    total_harga = models.PositiveIntegerField()
    waktu_checkout = models.DateTimeField(auto_now_add=True)
    kurir = models.ForeignKey(Courier, on_delete=models.SET_NULL, null=True)
    metode_pembayaran = models.ForeignKey(PaymentMethod, on_delete=models.SET_NULL, null=True)
    status_pengiriman = models.ForeignKey(StatusPengiriman, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f'{self.nama} pada {self.waktu_checkout}'

class ProdukAkanDikirim(models.Model):
    product = models.ForeignKey(AddProduk, on_delete=models.RESTRICT, related_name='produkakandikirim')
    detailpengiriman = models.ForeignKey(DetailPengiriman, on_delete=models.CASCADE, related_name='produkakandikirim')
    jumlah = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.detailpengiriman.nama} pada {self.detailpengiriman.waktu_checkout}'