from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from produk.models import AddProduk
from .models import ProdukDiKeranjang, ProdukAkanDikirim, DetailPengiriman, Visitor, PaymentMethod, StatusPengiriman, Courier
from .apps import KeranjangConfig
from .forms import FormPengiriman

class TestModel(TestCase):
    def setUp(self):
        self.produk = AddProduk.objects.create(
            nama="ramen",
            harga="20000",
            penjual="pucca",
            email="pucca@gmail.com",
            stok='2',
            deskripsi="ramen enak", 
            foto="https://source.unsplash.com/random/1920x1080")
        self.visitor = Visitor.objects.create()
        self.produk_di_keranjang = ProdukDiKeranjang.objects.create(
            product = self.produk,
            visitor = self.visitor,
            jumlah = '2'
        )
        self.kurir = Courier.objects.create(
            name = 'Gosend'
        )
        self.metode_pembayaran = PaymentMethod.objects.create(
            name = 'Gopay'
        )
        self.status_pengiriman = StatusPengiriman.objects.create(
            name = 'Menunggu Pembayaran'
        )
        self.detail_pengiriman = DetailPengiriman.objects.create(
            nama = 'pagi lord',
            telpon = '081234567890',
            email = 'pembeli@gmail.com',
            alamat = 'isekai',
            total_harga = '15000',
            kurir = self.kurir,
            metode_pembayaran = self.metode_pembayaran,
            status_pengiriman = self.status_pengiriman
            )
        self.produk_akan_dikirim = ProdukAkanDikirim.objects.create(
            product = self.produk,
            detailpengiriman = self.detail_pengiriman,
            jumlah = '2'
        )

    def test_model_produk_created(self):
        self.assertEqual(AddProduk.objects.all().count(), 1)
    
    def test_model_visitor_created(self):
        self.assertEqual(Visitor.objects.all().count(), 1)
    
    def test_model_produk_di_keranjang_created(self):
        self.assertEqual(Visitor.objects.all().count(), 1)
    
    def test_model_detail_pengiriman_created(self):
        self.assertEqual(Visitor.objects.all().count(), 1)
    
    def test_model_produk_akan_dikirim_created(self):
        self.assertEqual(ProdukAkanDikirim.objects.all().count(), 1)
    
    def test_model_kurir(self):
        self.assertEqual(Courier.objects.all().count(), 1)
        self.assertEqual(str(self.kurir), 'Gosend')

    def test_model_metode_pembayaran(self):
        self.assertEqual(PaymentMethod.objects.all().count(), 1)
        self.assertEqual(str(self.metode_pembayaran), 'Gopay')

    def test_model_status_pengiriman(self):
        self.assertEqual(StatusPengiriman.objects.all().count(), 1)
        self.assertEqual(str(self.status_pengiriman), 'Menunggu Pembayaran')

    def test_str_produk_di_keranjang(self):
        self.assertIn('ramen di ', str(self.produk_di_keranjang))
    
    def test_str_detail_pengiriman(self):
        self.assertIn('pagi lord pada ', str(self.detail_pengiriman))
        self.assertIn(':', str(self.detail_pengiriman))
        self.assertIn('-', str(self.detail_pengiriman))
    
    def test_str_produk_akan_dikirim(self):
        self.assertIn('pagi lord pada ', str(self.produk_akan_dikirim))
        self.assertIn(':', str(self.produk_akan_dikirim))
        self.assertIn('-', str(self.produk_akan_dikirim))

class TestUrls(TestCase):
    def setUp(self):
        self.produk = AddProduk.objects.create(
            nama="ramen",
            harga="20000",
            penjual="pucca",
            stok='2',
            email="pucca@gmail.com",
            deskripsi="ramen enak", 
            foto="https://source.unsplash.com/random/1920x1080")
        self.keranjang = reverse('keranjang:keranjang')
        self.checkout = reverse('keranjang:checkout')
        self.tambah = reverse('keranjang:tambah', args=[self.produk.id])
        self.addPOST = reverse('keranjang:addPOST')
        self.delPOST = reverse('keranjang:delPOST')
        self.incPOST = reverse('keranjang:incPOST')
        self.decPOST = reverse('keranjang:decPOST')
    
    def test_url_keranjang(self):
        self.assertEqual(self.keranjang, '/keranjang/')

    def test_url_checkout(self):
        self.assertEqual(self.checkout, '/keranjang/checkout')

    def test_url_tambah(self):
        self.assertEqual(self.tambah, '/keranjang/add/1')
    
    def test_url_addPOST(self):
        self.assertEqual(self.addPOST, '/keranjang/addPOST')

    def test_url_delPOST(self):
        self.assertEqual(self.delPOST, '/keranjang/delPOST')

    def test_url_incPOST(self):
        self.assertEqual(self.incPOST, '/keranjang/incPOST')

    def test_url_decPOST(self):
        self.assertEqual(self.decPOST, '/keranjang/decPOST')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(KeranjangConfig.name, 'keranjang')
        self.assertEqual(apps.get_app_config('keranjang').name, 'keranjang')

class TestForm(TestCase):
    def setUp(self):
        self.kurir = Courier.objects.create(
            name = 'Gosend'
        )
        self.metode_pembayaran = PaymentMethod.objects.create(
            name = 'Gopay'
        )
    def test_form_is_valid(self):
        form_pengiriman = FormPengiriman(data={
            "nama" : "Pucca",
            "telpon" : "081234567890",
            "email" : "ramen@gmail.com",
            "alamat" : "lengkap",
            "metode_pembayaran" : "1",
            "kurir" : "1",
        })
        self.assertTrue(form_pengiriman.is_valid())
        
    def test_form_invalid(self):
        form_pengiriman = FormPengiriman(data={})
        self.assertFalse(form_pengiriman.is_valid())

class TestViews(TestCase):
    def setUp(self):
        self.produk = AddProduk.objects.create(
            nama="ramen",
            harga="20000",
            penjual="pucca",
            stok='2',
            email="pucca@gmail.com",
            deskripsi="ramen enak", 
            foto="https://source.unsplash.com/random/1920x1080")
        self.kurir = Courier.objects.create(
            name = 'Gosend'
        )
        self.metode_pembayaran = PaymentMethod.objects.create(
            name = 'Gopay'
        )
        self.status_pengiriman = StatusPengiriman.objects.create(
            name = 'Menunggu Pembayaran'
        )
        self.client = Client()
        self.keranjang = reverse("keranjang:keranjang")
        self.checkout = reverse("keranjang:checkout")
        self.add = reverse('keranjang:tambah', args=[self.produk.id])
    
    def test_GET_keranjang(self):
        response = self.client.get(self.keranjang)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'keranjang/keranjang.html')

    def test_html_keranjang_empty(self):
        response = self.client.get(self.keranjang)
        self.assertContains(response, 'Belum ada produk ditambahkan')
        self.assertNotContains(response, 'Summary</h4>')
    
    def test_html_keranjang_with_produk(self):
        response = self.client.get('/keranjang/add/1')
        response = self.client.get(self.keranjang)
        self.assertContains(response, 'Shipping Details</h4>')
        self.assertContains(response, 'Summary</h4>')
        self.assertNotContains(response, 'Belum ada produk ditambahkan')
        self.assertContains(response, 'qty:')

    def test_GET_tambah_invalid(self):
        response = self.client.get('/keranjang/add/99')
        self.assertEqual(response.status_code, 400)
    
    def test_GET_tambah_valid(self):
        response = self.client.get('/keranjang/add/1')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(ProdukDiKeranjang.objects.get(id=1).product, self.produk)
    
    def test_GET_add(self):
        response = self.client.get('/keranjang/addPOST')
        self.assertEqual(response.status_code, 400)

    def test_POST_add_invalid_produk(self):
        response = self.client.post('/keranjang/addPOST', {'detail_id' : 99})
        self.assertEqual(response.status_code, 400)
    
    def test_POST_add_valid(self):
        response = self.client.post('/keranjang/addPOST', {'detail_id' : 1})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(ProdukDiKeranjang.objects.get(id=1).product, self.produk)
    
    def test_GET_del(self):
        response = self.client.get('/keranjang/delPOST')
        self.assertEqual(response.status_code, 400)

    def test_POST_del_invalid(self):
        response = self.client.post('/keranjang/delPOST', {'detail_id' : 99})
        self.assertEqual(response.status_code, 400)
    
    def test_POST_del_valid(self):
        response = self.client.post('/keranjang/addPOST', {'detail_id' : 1})
        response = self.client.post('/keranjang/delPOST', {'detail_id' : 1})
        self.assertEqual(ProdukDiKeranjang.objects.all().count(), 0)
    
    def test_GET_inc(self):
        response = self.client.get('/keranjang/incPOST')
        self.assertEqual(response.status_code, 400)
    
    def test_POST_inc_invalid(self):
        response = self.client.post('/keranjang/incPOST', {'detail_id' : 1})
        self.assertEqual(response.status_code, 400)

        response = self.client.post('/keranjang/incPOST', {'detail_id' : 9})
        self.assertEqual(response.status_code, 400)
        
    def test_POST_inc_valid(self):
        response = self.client.post('/keranjang/addPOST', {'detail_id' : 1})
        response = self.client.post('/keranjang/incPOST', {'detail_id' : 1})
        visitor = Visitor.objects.last()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(ProdukDiKeranjang.objects.get(product=self.produk, visitor=visitor).jumlah, 2)

        response = self.client.post('/keranjang/incPOST', {'detail_id' : 1})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(ProdukDiKeranjang.objects.get(product=self.produk, visitor=visitor).jumlah, 2)

    def test_GET_dec(self):
        response = self.client.get('/keranjang/decPOST')
        self.assertEqual(response.status_code, 400)
    
    def test_POST_dec_invalid(self):
        response = self.client.post('/keranjang/decPOST', {'detail_id' : 1})
        self.assertEqual(response.status_code, 400)

        response = self.client.post('/keranjang/decPOST', {'detail_id' : 9})
        self.assertEqual(response.status_code, 400)
        
    def test_POST_dec_valid(self):
        response = self.client.post('/keranjang/addPOST', {'detail_id' : 1})
        response = self.client.post('/keranjang/incPOST', {'detail_id' : 1})
        visitor = Visitor.objects.last()

        response = self.client.post('/keranjang/decPOST', {'detail_id' : 1})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(ProdukDiKeranjang.objects.get(product=self.produk, visitor=visitor).jumlah, 1)

        response = self.client.post('/keranjang/decPOST', {'detail_id' : 1})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(ProdukDiKeranjang.objects.get(product=self.produk, visitor=visitor).jumlah, 1)
    
    def test_GET_checkout(self):
        response = self.client.get('/keranjang/checkout')
        self.assertEqual(response.status_code, 302)

    def test_POST_checkout_with_no_produk_di_keranjang(self):
        response = self.client.post(
            '/keranjang/checkout',
            {
                'nama' : 'budi setiawan',
                'telpon' : '081234567890',
                'email' : 'budi@set.com',
                'alamat' : 'abcd',
                'metode_pembayaran' : '1',
                'kurir' : '1'
            }
        )
        self.assertEqual(response.status_code, 400)

    def test_POST_checkout_with_produk_di_keranjang(self):
        form_data = {
            'nama' : 'budi setiawan',
            'telpon' : '081234567890',
            'email' : 'budi@set.com',
            'alamat' : 'abcd',
            'metode_pembayaran' : '1',
            'kurir' : '1'
        }

        response = self.client.get('/keranjang/add/1')        
        response = self.client.post('/keranjang/checkout', form_data)
        visitor = Visitor.objects.last()
        detail_pengiriman = DetailPengiriman.objects.get(id=1)

        self.assertTemplateUsed(response, 'keranjang/checkout.html')
        self.assertEqual(ProdukDiKeranjang.objects.filter(visitor=visitor).count(), 0)
        self.assertEqual(ProdukAkanDikirim.objects.filter(detailpengiriman=detail_pengiriman).count(), 1)
        self.assertEqual(AddProduk.objects.get(id=1).stok, 1)
