from django.db import models

# Create your models here.
class AddProduk(models.Model):
    nama = models.CharField(max_length=50, blank = False)
    # foto = models.ImageField(upload_to='static/img/',  default='img/logo.png')
    foto = models.TextField(default='https://mmc.tirto.id/image/otf/500x0/2019/10/18/pppk_ratio-16x9.jpg')
    # harga = models.CharField(max_length=300)
    harga = models.PositiveIntegerField()
    stok = models.IntegerField(default='1')
    penjual = models.CharField(max_length=50)
    email = models.EmailField(max_length=254)
    deskripsi = models.TextField()

    def __str__(self):
        return self.nama
