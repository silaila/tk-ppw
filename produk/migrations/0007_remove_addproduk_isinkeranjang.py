# Generated by Django 3.1.2 on 2020-10-30 07:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('produk', '0006_addproduk_isinkeranjang'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='addproduk',
            name='isInKeranjang',
        ),
    ]
