from django.test import TestCase, Client
from django.urls import resolve,reverse
from django.apps import apps
from .models import AddProduk
from . import views 
# from .views import listproduk, addproduk
from .forms import FromAdd
from .apps import ProdukConfig
# Create your tests here.

class TestModel(TestCase):
    def setUp(self):
        self.produk = AddProduk.objects.create(nama="ramen",harga="20000", stok="2", penjual="pucca",email="pucca@gmail.com",deskripsi="ramen enak", foto="https://source.unsplash.com/random/1920x1080")

    def test_model_produk_created(self):
        self.assertEqual(AddProduk.objects.all().count(), 1)

    def test_str(self):
        self.assertEqual(str(self.produk), "ramen")

class FormTest(TestCase):
    def test_form_is_valid(self):
        form_barang = FromAdd(data={
            "penjual" : "Pucca",
            "email" : "pucca@gmail.com",
            "nama" : "ramen",
            "harga" : "50000",
            "stok" : "2",
            "deskripsi" : "ramen made by love",
            "foto" : "https://source.unsplash.com/random/1920x1080"
        })
        self.assertTrue(form_barang.is_valid())
        
    def test_form_invalid(self):
        form_barang = FromAdd(data={})
        self.assertFalse(form_barang.is_valid())

class TestUrls(TestCase):
    def setUp(self):
        self.produk = AddProduk.objects.create(nama="A",harga="20000",stok="2", penjual="B",email="B@gmail.com",deskripsi="Barang")
        self.addproduk = reverse('produk:addproduk')
        self.listproduk = reverse('produk:listproduk')

    def test_urls_addproduk_test(self):
        found = resolve(self.addproduk)
        self.assertEqual(found.func, views.addproduk)

    def test_urls_listproduk_test(self):
        found = resolve(self.listproduk)
        self.assertEqual(found.func, views.listproduk)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.listproduk = reverse("produk:listproduk")
        self.addproduk = reverse("produk:addproduk") 

    def test_GET_addproduk(self):
        response = self.client.get(self.addproduk)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addproduk.html')

    def test_POST_addProduk(self):
        response = self.client.post(self.addproduk,
                                    {
                                        "penjual" : "Pucca",
                                        "email" : "pucca@gmail.com",
                                        "nama" : "ramen",
                                        "stok" : "2",
                                        "harga" : "50000",
                                        "deskripsi" : "ramen made by love",
                                        "foto" : "https://source.unsplash.com/random/1920x1080"
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_addproduk_invalid(self):
        response = self.client.post(self.addproduk,
                                    {
                                        "penjual" : "Pucca",
                                        "email" : "pucca@gmail.com",
                                        "nama" : "ramen",
                                        "harga" : "50000",
                                        "deskripsi" : "ramen made by love",
                                        "foto" : "https://source.unsplash.com/random/1920x1080"
                                    }, follow=True)
        self.assertTemplateUsed(response, 'listproduk.html')

    def test_GET_listproduk(self):
        response = self.client.get(self.listproduk)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'listproduk.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(ProdukConfig.name, 'produk')
        self.assertEqual(apps.get_app_config('produk').name, 'produk')