from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from .forms import FromAdd
from .models import AddProduk

# Create your views here.
def listproduk(request):
    if request.method =="POST" and request.POST.get("carinama") !=None:
        search = request.POST.get("carinama")
        produk = AddProduk.objects.filter(nama__contains=search)
        response = {
            'produk' : produk,
        }
        return render(request,'listproduk.html', response)
    else:
        produk = AddProduk.objects.all()
        response = {
            'produk' : produk,
        }
        return render(request,'listproduk.html', response)

    # except:
        # return redirect('/listproduk/addproduct')
    

def addproduk(request) :
    if request.method == "POST":
        form = FromAdd(request.POST)
        if (form.is_valid()):
            produk = AddProduk()
            produk.penjual = form.cleaned_data['penjual']
            produk.email = form.cleaned_data['email']
            produk.nama = form.cleaned_data['nama']
            produk.harga = form.cleaned_data['harga']
            produk.stok = form.cleaned_data['stok']
            produk.deskripsi = form.cleaned_data['deskripsi']
            produk.foto = form.cleaned_data['foto']
            produk.save()
            print(produk)
        return redirect ("/listproduk/")
        return render(request, "addproduk.html", {"form" : form})
    # else : 
    form = FromAdd()
    return render(request, "addproduk.html", {"form" : form})