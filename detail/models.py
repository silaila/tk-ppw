from django.db import models

# Create your models here.
class Testimoni(models.Model):
    name = models.CharField(max_length=50)
    komentar = models.CharField(max_length=200)
    produk = models.ForeignKey("produk.AddProduk", on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return "{}".format(self.name)