from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'detail'

urlpatterns = [
    path('admin/', admin.site.urls),
    path("<int:detail_id>", views.detailproduk, name='detailproduk')
    # yon yang detail produk tambahin id ya buat nge link ke produk tertentu yang dia mau
]
#landing pagenya ke list produk, ngambl dr models yg ada di list produk buat detail sm testi